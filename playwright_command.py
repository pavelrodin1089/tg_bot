from playwright.async_api import Page, async_playwright
import time
import asyncio
from browser import Browser


async def get_yoga_schedule():
    browser = await Browser().get_page()
    page = await browser.new_page()
    await page.goto(url="https://art-yoga.ru/")
    state = await page.context.storage_state()
    print(f"{state = }")
    print(f"{state['origins'] = }")
    await page.locator("a[href='/raspisanie']").first.click()
    # await page.frame_locator("#personal_widget_frame_d85").get_by_text(
    #     "Динамическая хатха-йога 17:00 - 18:30 Art Yoga Мостовая Аделя Places available: ").click()
    # await page.frame_locator("#personal_widget_frame_d85").get_by_role("textbox", name="912 345-67-89").click()
    # await page.frame_locator("#personal_widget_frame_d85").get_by_role("textbox", name="912 345-67-89").fill("9265902841")
    # await page.frame_locator("#personal_widget_frame_d85").locator("//input[@type='checkbox']").first.check()
    # await page.frame_locator("#personal_widget_frame_d85").get_by_role("dialog", name="Динамическая хатха-йога  17:00").click(modifiers=["Control"])
    # await page.frame_locator("#personal_widget_frame_d85").get_by_role("link", name="Login").click()
    time.sleep(90)
    state = await page.context.storage_state()
    print(f"{state['origins'] = }")
    print(f"{state['origins'][1] = }")
    # await page.close()

coro = get_yoga_schedule()
asyncio.run(coro)