from playwright.async_api import Page, async_playwright
import requests
import time
import asyncio


class Browser:
    async def get_page(self):
        # r = requests.post('https://mobifitness.ru/api/v8/oauth/access_token',
        #                   data={'response_type': 'token', "client_type": "2", "client_id": 581509})
        # print("This is r", r.content)
        playwright = await async_playwright().start()
        browser = await playwright.chromium.launch_persistent_context(user_data_dir="~/.config/chromium",
                                                                      headless=False)
        # browser = await playwright.chromium.launch_persistent_context(user_data_dir="~/.config/chromium/Default",
        #                                                               base_url="https://art-yoga.ru/", headless=False)
        # page = await browser.new_page()
        # cookies = await page.context.cookies()
        # for cookie in cookies:
        #     print(cookie)

        return browser
